
$(document).ready(function () {
	
	$(".edit").hide();
	$("#edittable").click(function () {
		$(".normal").toggle();
		$(".edit").toggle();
	});
	
	
	$(".checked").prop("checked", true);
	$(".disabled input[type='radio']").prop("disabled", true);
	
	$('.downloadbtn input:checkbox').change(function(){
		if($(this).is(":checked")) {
			$(this).closest('div').find('.radios').removeClass('disabled');
			$(this).closest('div').find('input[type="radio"]').prop("disabled", false);
		} else {
			$(this).closest('div').find('.radios').addClass('disabled');
			$(this).closest('div').find('input[type="radio"]').prop("disabled", true);
		}
	});
	
	$('#buttonlist a').click(function() {
		var elementClassName = $(this).attr('id');
		$('div.'+elementClassName).slideToggle(300);
		$('.noshow').not('div.'+elementClassName).slideUp(300);
		$('a').not(this).removeClass('active');
		$(this).toggleClass('active');
		$('html, body').animate({
			scrollTop: $('.or').offset().top
		}, '');
		return false;
	});
	$('.noshow .closebox').click(function() {
		$('.noshow').slideUp(300);
		$('#buttonlist a').not(this).removeClass('active');
		return false;
	});
	
	$(".hidethis").hide();
	$(".collapse-btn").click(function () {
		$(this).parents(".structure-row").siblings().children(".hidethis, .hidethat").removeClass("active").slideUp(500);
		$(this).parents(".structure-row").find(".hidethat").slideUp(500, function(){
			$(this).parents(".structure-row").find(".hidethis").slideToggle(500);
		});
	});
	
	$(".hidethat").hide();
	$(".edit-btn").click(function () {
		$(this).parents(".structure-row").siblings().children(".hidethis, .hidethat").removeClass("active").slideUp(500);
		$(this).parents(".structure-row").find(".hidethis").slideUp(500, function(){
			$(this).parents(".structure-row").find(".hidethat").toggleClass("active").slideToggle(500);
		});
	});
	
	$(".fetch-btn").click(function () {
		$(".hidethat").slideDown(500);
	});

	
	$(".optionsbutton").click(function () {
		$(".hidethis").slideToggle(500);
		return false;
	});
	
	$(".generated").hide();
	$(".savebutton").click(function () {
		$(".generated").slideDown(500);
		$(".savebutton").prop("disabled", true);
		return false;
	});

	$("#select2").hide();
	$("#select1").change(function(){
		if($(this).val() == 7){
			$("#select2").show();
		}else{
			$("#select2").hide();
		}
	});
	
	$('#mySelector').change( function(){
		var selection = $(this).val();
		var dataset = $('#myTable tbody').find('tr');

		dataset.each(function(index) {
		item = $(this);
		item.hide();

		var firstTd = item.find('td:first-child');
		var text = firstTd.text();
		var ids = text.split(',');

		for (var i = 0; i < ids.length; i++)
			{
				if (ids[i] == selection)
				{
					item.show();
				}
			}
		});
	});
	
	
	$(".transfer").hide();
	$(".upload").hide();
	
	$(".btntransfer").click(function () {
		$(".transfer").show();
		$(".btnupload").addClass("pt-disabled");
		return false;
	});
	
	$(".btnupload").click(function () {
		$(".upload").show();
		$(".btntransfer").addClass("pt-disabled");
		return false;
	});

	$(".editing").hide();
	$("#r9o").click(function () {
		if ($(this).is(":checked")) {
			$(".editing").show();
			$(".original").hide();
		} else {
			$(".editing").hide();
			$(".original").show();
		}
	});
	$("#r9v").click(function () {
		if ($(this).is(":checked")) {
			$("#r9o").prop('checked', true);
			$(".editing").show();
			$(".original").hide();
		} else {
			
		}
	});
	
	$(".artefacts").hide();
	$("#r10o").click(function () {
		if ($(this).is(":checked")) {
			$(".artefacts").show();
		} else {
			$(".artefacts").hide();
		}
	});
	
	$(".collapse-row").hide();
	
	$(".collapse-table").click(function (){
		$(".collapse-row").slideToggle();
		return false;
	});
	
	$(".descr").hide();
	$(".collapse-descr").click(function (){
		$(".descr").slideToggle();
		return false;
	});

	$(".checkAll").on('change',function() {
		$(this).closest('table').find('input:checkbox').not(this).prop('checked', this.checked);
	});
	
	$("input[type=checkbox]").each(function () {
		$(this).change(updateCount);
	});

	updateCount();

	function updateCount () {
		var count = $("input[type=checkbox]:checked").not(".checkAll, #r9o, #r10o, #r9v, #r10v, #r11v").size();

		$("#count").text(count);
		$("#status").toggle(count > 0);
	};

	var lengthText = 10;
	var text = $('.trunk').text();
	var shortText = $.trim(text).substring(0, lengthText).split(" ").slice(0, -1).join(" ") + "...";
	$('.trunk').text(shortText);

	$('.trunk').hover(function(){
		$(this).text(text).animate();
	}, function(){
		$(this).text(shortText);
	});
	
	$(".concept-show1, .concept-show2").hide();
	$(".concept-list a.reference").click(function (){
		$(".concept-list").hide();
		$(".concept-show1").show();
		return false;
	});
	$(".concept-list a.frequency").click(function (){
		$(".concept-list").hide();
		$(".concept-show2").show();
		return false;
	});
	$("#overlay").hide();
	$(".concept-list-edit a.reference").click(function (){
		$(".concept-list-edit").hide();
		$("#overlay").show();
		$(".visible").addClass("zindex");
		$(".concept-show1").show();
		return false;
	});
	$(".concept-list-edit a.frequency").click(function (){
		$(".concept-list-edit").hide();
		$("#overlay").show();
		$(".visible").addClass("zindex");
		$(".concept-show2").show();
		return false;
	});
	$(".concept-show1 a.apply, .concept-show2 a.apply").click(function (){
		$(".concept-list-edit").show();
		$("#overlay").hide();
		$(".visible").removeClass("zindex");
		$(".concept-show1, .concept-show2").hide();
		return false;
	});
	
	$(".concept-show1 a.cclose, .concept-show2 a.cclose").click(function (){
		$(".concept-list").show();
		$(".concept-show1, .concept-show2").hide();
		return false;
	});
	
	
	
	$(".collapse-header").click(function () {
		$header = $(this);
		$collapse = $header.next();
		$collapse.slideToggle(500);
		if($(this).children('.pt-icon-standard').hasClass('pt-icon-chevron-up')) {
			$(this).children('.collapse-header .pt-icon-standard').removeClass('pt-icon-chevron-up');
			$(this).children('.collapse-header .pt-icon-standard').addClass('pt-icon-chevron-down');
		} else {
			$(this).children('.collapse-header .pt-icon-standard').addClass('pt-icon-chevron-up');
			$(this).children('.collapse-header .pt-icon-standard').removeClass('pt-icon-chevron-down');
		}
	});
	
	
	
	$('.collapse').on('shown.bs.collapse', function(){
		$(this).prev().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
	}).on('hidden.bs.collapse', function(){
		$(this).prev().find(".glyphicon-chevron-up").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
	});
	
	$("[name=toggler]").click(function(){
		$('.toHide').hide();
		$("#blk-"+$(this).val()).show('slow');
	});
	
	
	
	/*$("#top").hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) {
			$("#top").fadeIn("slow");
		} else {
			$("#top").fadeOut("slow");
		}
	});

	$("a[href=#top], #top").click(function () {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});*/
	
	
	
	
	
	
	
});

var floatPanel = new McFloatPanel();
/* Float Panel v2016.10.28. Copyright www.menucool.com */
function McFloatPanel(){var i=[],s=[],h="className",t="getElementsByClassName",d="length",l="display",C="transition",m="style",B="height",c="scrollTop",k="offsetHeight",a="fixed",e=document,b=document.documentElement,j=function(a,c,b){if(a.addEventListener)a.addEventListener(c,b,false);else a.attachEvent&&a.attachEvent("on"+c,b)},o=function(c,d){if(typeof getComputedStyle!="undefined")var b=getComputedStyle(c,null);else b=c.currentStyle;return b?b[d]:a},L=function(){var a=e.body;return Math.max(a.scrollHeight,a[k],b.clientHeight,b.scrollHeight,b[k])},O=function(a,c){var b=a[d];while(b--)if(a[b]===c)return true;return false},g=function(b,a){return O(b[h].split(" "),a)},q=function(a,b){if(!g(a,b))if(!a[h])a[h]=b;else a[h]+=" "+b},p=function(a,f){if(a[h]&&g(a,f)){for(var e="",c=a[h].split(" "),b=0,i=c[d];b<i;b++)if(c[b]!==f)e+=c[b]+" ";a[h]=e.replace(/^\s+|\s+$/g,"")}},n=function(){return window.pageYOffset||b[c]},z=function(a){return a.getBoundingClientRect().top},F=function(b){var c=n();if(c>b.oS&&!g(b,a))q(b,a);else g(b,a)&&c<b.oS&&p(b,a)},x=function(){for(var a=0;a<s[d];a++)J(s[a])},J=function(a){if(a.oS){a.fT&&clearTimeout(a.fT);a.fT=setTimeout(function(){if(a.aF)F(a);else y(a)},50)}else y(a)},w=function(d,c,b){p(d,a);c[l]="none";b.position=b.top=""},y=function(c){var j=z(c),f=c[k],e=c[m],d=c.pH[m],h=n();if(j<c.oT&&h>c.oS&&!g(c,a)&&(window.innerHeight||b.clientHeight)>f){c.tP=h+j-c.oT;var p=L();if(f>p-c.tP-f)var i=f;else i=0;d[l]="block";d[C]="none";d[B]=f+1+"px";c.pH[k];d[C]="height .3s";d[B]=i+"px";q(c,a);e.position=a;e.top=c.oT+"px";if(o(c,"position")!=a)d[l]="none"}else if(g(c,a)&&(h<c.tP||h<c.oS)){var s=o(c,"animation");if(c.oS&&c.classList&&s.indexOf("slide-down")!=-1){var r=o(c,"animationDuration");c.classList.remove(a);e.animationDirection="reverse";e.animationDuration="300ms";void c[k];c.classList.add(a);setTimeout(function(){w(c,d,e);e.animationDirection="normal";e.animationDuration=r},300)}else w(c,d,e)}},I=function(){var f=[],c,b;if(e[t]){f=e[t]("float-panel");i=e[t]("slideanim")}else{var k=e.getElementsByTagName("*");c=k[d];while(c--)g(k[c],"float-panel")&&f.push(k[c])}c=f[d];for(var h=0;h<c;h++){b=s[h]=f[h];b.oT=parseInt(b.getAttribute("data-top")||0);b.oS=parseInt(b.getAttribute("data-scroll")||0);if(b.oS>20&&o(b,"position")==a)b.aF=1;b.pH=e.createElement("div");b.pH[m].width=b.offsetWidth+"px";b.pH[m][l]="none";b.parentNode.insertBefore(b.pH,b.nextSibling)}if(s[d]){setTimeout(x,160);j(window,"scroll",x)}},f,D=200,E=0,r,u,H=function(){return window.innerWidth||b.clientWidth||e.body.clientWidth};function K(){if(!r)r=setInterval(function(){var a=e.body;if(a[c]<3)a[c]=0;else a[c]=a[c]/1.3;if(b[c]<3)b[c]=0;else b[c]=b[c]/1.3;if(!n()){clearInterval(r);r=null}},14)}function A(){clearTimeout(u);if(n()>D&&H()>E){u=setTimeout(function(){p(f,"mcOut")},60);f[m][l]="block"}else{q(f,"mcOut");u=setTimeout(function(){f[m][l]="none"},500)}}var N=function(){f=e.getElementById("backtop");if(f){var a=f.getAttribute("data-v-w");if(a){a=a.replace(/\s/g,"").split(",");D=parseInt(a[0]);if(a[d]>1)E=parseInt(a[1])}j(f,"click",K);j(window,"scroll",A);A()}},v=function(){for(var c=n(),e=c+window.innerHeight,g=i[d],b,f,a=0;a<g;a++){b=c+z(i[a]),f=b+i[a][k];if(b<e)q(i[a],"slide");else p(i[a],"slide")}},G=function(){if(i[d]){j(window,"scroll",v);v()}},M=function(){I();N();G()};j(window,"load",M);j(document,"touchstart",function(){})}

function myFunction() {
	document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
	if (!event.target.matches('.dropbtn')) {
		var dropdowns = document.getElementsByClassName("dropdown-content");
		var i;
		for (i = 0; i < dropdowns.length; i++) {
			var openDropdown = dropdowns[i];
			if (openDropdown.classList.contains('show')) {
				openDropdown.classList.remove('show');
			}
		}
	}
}
function myFunction2() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
function myFunction3() {
  /* Get the text field */
  var copyText = document.getElementById("myInput3");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}
