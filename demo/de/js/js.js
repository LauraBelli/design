$(function () {
	//getting click event to show modal
	$('.location-list-link').click(function () {
		$('#dialog_confirm_map').modal();
		//appending modal background inside the bigform-content
		$('.modal-backdrop').appendTo('.indicator-chart-wrapper');
		//removing body classes to able click events
		$('body').removeClass();
	});
});
$(document).ready(function() {
	
	$('.government td').mouseover(function () {
		$(this).siblings().not('th').css('background-color', '#fcf8e3');
		var ind = $(this).index();
		$('td:nth-child(' + (ind + 1) + ')').css('background-color', '#fcf8e3');
	});
	$('.government td').mouseleave(function () {
		$(this).siblings().not('th').css('background-color', '');
		var ind = $(this).index();
		$('td:nth-child(' + (ind + 1) + ')').css('background-color', '');
	});

	$('table.government tbody tr td').click(function(){
		$('table tbody tr td.highlighted').removeClass('highlighted');
		$(this).addClass('highlighted');
	});
	
	$('#test').BootSideMenu({
		side: "right",
		pushBody: true,
		width: '360px',
		autoClose: true
	});
	
	$('.closeall').click(function(){
	  $('#accordion2 .panel-collapse.in')
		.collapse('hide');
	});
	$('.openall').click(function(){
	  $('.panel-collapse:not(".in")')
		.collapse('show');
	});
	
	$('[data-toggle="tooltip"]').tooltip();
	
	$("#example td.content").click(function() {     
		$('td').removeClass('selected');
		$(this).addClass('selected');
		var column_num = parseInt( $(this).index() ) + 1;
		var row_num = parseInt( $(this).parent().index() )+1;

		$("#result").html( "Row_num =" + row_num + "  ,  Rolumn_num ="+ column_num );   
	});
	
	$('.collapse.in').prev().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
	$('.collapse').on('shown.bs.collapse', function(){
		$(this).prev().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
	}).on('hidden.bs.collapse', function(){
		$(this).prev().find(".glyphicon-chevron-up").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
	});
});