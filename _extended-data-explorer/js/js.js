

$(document).ready(function() {
	
	$("#top").hide();
	$('#main').scroll(function () {
		if ($(this).scrollTop() > 140) { // shows the top button in the right bottom corner when needed
			$("#top").fadeIn("slow");
		} else {
			$("#top").fadeOut("slow");
		}
	});
	
	$("a[href=#top], #top").click(function(){
		$("#main").animate({scrollTop:0}, "slow");
		return false;
	});
	
	
	$(".list-group .list-group-item").click(function() {
		$('.list-group .list-group-item.active').removeClass('active');
		$(this).addClass('active');

	});
	
	$('.accordion a i.arrow').hide();
	
	$('.accordion a[data-toggle="collapse"]').click(function() {
		var clickedBtnID = $(this).attr('href');
		/*alert(clickedBtnID);*/
		$(this).removeClass('active');
		$('.accordion a[data-toggle="collapse"] i.arrow').hide();
		
		$('.panel-group').on('show.bs.collapse', function() {
			$('.accordion a[data-toggle="collapse"]').removeClass('active');
			$('.accordion a[data-toggle="collapse"] i.arrow').hide();
			$('.accordion a[href="'+clickedBtnID+'"]').addClass('active');
			$('.accordion a[href="'+clickedBtnID+'"] i.arrow').show();
		});
	});
	
	$('.chart-type').hide();
	$('a[href="#chart-tab"]').on('shown.bs.tab', function () {
		$('.chart-type').show();
	});
	$('a[href="#table-tab"]').on('shown.bs.tab', function () {
		$('.chart-type').hide();
	});
	
	var e = $('.col');
	for (var i = 0; i < 25; i++) {
		e.clone().insertAfter(e);
	}
	
	$('#collapseExample').on('shown.bs.collapse', function () {
		$('#main').animate({scrollTop: $("#collapseExample").offset().top - 155}, 600);
		$('#collapseExample a.close').click(function(){
			$('#main').animate({scrollTop: 0}, 600);
			$(this).collapse('hide');
		});
	});
	
	$('.close').click(function() {
		$('#collapseExample').collapse('hide');
	});
	
});